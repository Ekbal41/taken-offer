jQuery(document).ready(function ($) {

  // Responsive width calculation
  function calculateResponsiveWidth() {
    var windowWidth = $(window).width();
    if (windowWidth >= 992) {
      return "800px"; // Laptop width
    } else if (windowWidth >= 576) {
      return "700px"; // Tablet width
    } else {
      return "400px"; // Default width
    }
  }

  function minimizeWindowWidth() {
    var windowWidth = $(window).width();
    if (windowWidth >= 992) {
      return "500px"; // Laptop width
    } else if (windowWidth >= 576) {
      return "500px"; // Tablet width
    } else {
      return "400px"; // Default width
    }
  }

  let windowCount = 0;
  function handleWindowActions($window) {
    windowCount++;
  }
  $(document).on("click", ".window-min", function () {
    console.log("minimize");
    var $window = $(this).closest(".window");

    $window.find(".window-body").slideUp(200, function () {
      $window.css({
        position: "fixed",
        "border-radius": "20px",
        width: minimizeWindowWidth(),
        transition: "width 0.3s ease-in-out",
      });
      $window.find(".window-header").css("border-radius", "20px");
      $window.animate({ right: "0", top: "91vh" });
    });
  });

  $(document).on("click", ".window-max", function () {
    var $window = $(this).closest(".window");
    $window.animate({ right: 0, left: 0, top: "20%" }, function () {
      $window.css({
        position: "fixed",
        "border-radius": "0",
        width: calculateResponsiveWidth(),
        transition: "width 0.3s ease-in-out",
      });
      $window.find(".window-body").slideDown(200);
    });
  });

  $(document).on("click", ".window-close", function () {
    $(this).closest(".window").fadeOut();
  });

  $(".window").draggable({
    containment: "tab",
  });

  // Update window width on window resize
  $(window).resize(function () {
    var $window = $(".window");
    if ($window.is(":visible")) {
      $window.css("width", calculateResponsiveWidth());
    }
  });

  $(".window-open-btn").click(function () {
    var target = $(this).data("window-target");
    $(target).css({
      display: "block",
      position: "fixed",
      width: calculateResponsiveWidth(),
    });
    handleWindowActions($(target));
  });


});